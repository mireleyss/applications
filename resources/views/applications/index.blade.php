<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>Applications</title>
</head>
<body>

<h1>Applications</h1>

<a href="{{ route('applications.create') }}">Add application</a>
<a href="{{ route('users.index') }}">View all users</a>

@if($applications->isEmpty())
    <p>No applications found</p>
@else
    <table>
        <thead>
        <tr>
            <th>Header</th>
            <th>Description</th>
            <th>Priority</th>
            <th>User</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($applications as $application)
            <tr>
                <td>
                    {{ $application->header }}
                </td>
                <td>
                    @if(!$application->description)
                        <a>No desc</a>
                    @else
                        {{ $application->description }}
                    @endif
                </td>
                <td>
                    {{ $application->priority->name }}
                </td>
                <td>
                    <a href="{{ route('users.show', $application->user->id) }}">{{ $application->user->name }}</a>
                </td>
                <td>
                    {{ $application->status->name }}
                </td>
                <td>
                    <a href="{{ route('applications.edit', $application->id) }}">Edit</a>
                </td>
                <td>
                    <form action="{{ route('applications.destroy', $application) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" onclick="return confirm('Are you sure you want to delete this application? {{ $application->header }}')">
                            Delete
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
</body>
</html>
