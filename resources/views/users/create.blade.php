<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>Document</title>
</head>
<body>

    <h1>Create user</h1>

    <form action=" {{ route('users.store') }} " method="POST">
        @csrf

        <div>
            <label for="name">Name</label>
            <input type="text" name="name">
        </div>

        <div>
            <label for="email">Email</label>
            <input type="email" name="email">
        </div>

        <button type="submit">Create</button>
    </form>
    <a href="{{ route('users.index') }}">Cancel</a>

</body>
</html>
