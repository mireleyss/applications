<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>Document</title>
</head>
<body>
<h1>User: {{ $user->name }}</h1>

<div>
    <a>Name: {{ $user->name }}</a>
</div>

<div>
    <a>Email: {{ $user->email }}</a>
</div>

<a href="{{ route('applications.index') }}">Back</a>
<a href="{{ route('users.edit', $user) }}">Edit user</a>

</body>
</html>
