<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>{{ $user->title }}: edit</title>
</head>
<body>

<h1>Edit user</h1>

<form action="{{ route('users.update', $user) }}" method="POST" class="data-input">
    @csrf
    @method('PUT')

    <div>
        <label>Name</label>
        <input type="text" name="name" value="{{ $user->name }}">
    </div>

    <div>
        <label>Email</label>
        <input type="email" name="email" value="{{ $user->email }}">
    </div>

    <button type="submit">Save</button>
</form>
<a href="{{ route('users.index') }}">Cancel</a>

</body>
</html>
