<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Application;
use App\Models\Priority;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function index() {
        $applications = Application::all();
        return view('applications.index', compact('applications'));
    }

    public function create() {
        $users = User::all();
        $statuses = Status::all();
        $priorities = Priority::all();
        return view('applications.create', compact('users', 'statuses', 'priorities'));
    }

    public function store(Request $request) {
        $request->validate([
            'header' => 'max:50',
            'description' => 'max:300',
            'priority_id' => 'required',
            'user_id' => 'required',
            'status_id' => 'required',
        ]);

        Application::create($request->all());

        return redirect()->route('applications.index');
    }

    public function edit(Application $application) {
        $users = User::all();
        $statuses = Status::all();
        $priorities = Priority::all();
        return view('applications.edit', compact('application', 'users', 'statuses', 'priorities'));
    }

    public function update(Request $request, Application $application) {
        $request->validate([
            'header' => 'max:50',
            'description' => 'max:300',
            'priority_id' => 'required',
            'user_id' => 'required',
            'status_id' => 'required',
        ]);

        $application->update($request->all());

        return redirect()->route('applications.index');
    }

    public function destroy(Application $application)
    {
        $application->delete();
        return redirect()->route('applications.index');
    }
}
