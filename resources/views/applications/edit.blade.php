<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>Add task</title>
</head>
<body>

<h1>Edit application</h1>

<form action="{{ route('applications.update', $application) }}" method="POST">
    @csrf
    @method('PUT')

    <div>
        <label>Header</label>
        <input type="text" name="header" placeholder="Header" value="{{ $application->header}} ">
    </div>

    <div>
        <label>Description</label>
        <textarea name="description" placeholder="Description">{{ $application->description }}</textarea>
    </div>
    <div>
        <label for="priority_id">Priority</label>
        <select name="priority_id" id="priority_id">
            <option disabled>Choose level of priority</option>
            @foreach($priorities as $priority)
                <option value="{{ $priority->id }}" {{ $priority->id == $application->priority_id ? 'selected' : '' }}>{{ $priority->name }}</option>
            @endforeach
        </select>
    </div>

    <div>
        <label for="user_id">User</label>
        <select name="user_id" id="user_id">
            <option disabled>Choose user</option>
            @foreach($users as $user)
                <option value="{{ $user->id }}" {{ $user->id == $application->user_id ? 'selected' : '' }}>{{ $user->name }}</option>
            @endforeach
        </select>
    </div>

    <div>
        <label for="status_id">Status</label>
        <select name="status_id" id="status_id">
            <option disabled>Choose user</option>
            @foreach($statuses as $status)
                <option value="{{ $status->id }}" {{ $status->id == $application->status_id ? 'selected' : '' }}>{{ $status->name }}</option>
            @endforeach
        </select>
    </div>

    <button type="submit">Save</button>
</form>
<a href="{{ route('applications.index') }}">Cancel</a>
</body>
</html>
